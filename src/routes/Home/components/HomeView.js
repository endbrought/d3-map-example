import React from 'react'
import { feature, mesh } from 'topojson'
// import { Map, PolygonGroup } from 'react-d3-map'
// import DuckImage from '../assets/Duck.jpg'
import { geoPath, geoAlbersUsa } from 'd3-geo'
import { select } from 'd3-selection'
// import queue from 'queue'
// import data from '../assets/states.json'
import usData from '../assets/us.json'
import congressData from '../assets/us-congress-113.json'
import './HomeView.scss'

const width = 960
const height = 600

const projection = geoAlbersUsa()
    .scale(1280)
    .translate([width / 2, height / 2])

const path = geoPath()
    .projection(projection)

const svg = select('body').append('svg')
    .attr('width', width)
    .attr('height', height)

function ready (error, us, congress) {
  if (error) throw error

  console.log(us)

  svg.append('defs').append('path')
      .attr('id', 'land')
      .datum(feature(us, us.objects.land))
      .attr('d', path)

  svg.append('clipPath')
      .attr('id', 'clip-land')
    .append('use')
      .attr('xlink:href', '#land')

  svg.append('g')
      .attr('class', 'districts')
      .attr('clip-path', 'url(#clip-land)')
    .selectAll('path')
      .data(feature(congress, congress.objects.districts).features)
    .enter().append('path')
      .attr('d', path)
    .append('title')
      .text(function (d) { return d.id })

  svg.append('path')
      .attr('class', 'district-boundaries')
    .datum(mesh(congress, congress.objects.districts, function (a, b) {
      return a !== b && (a.id / 1000 | 0) === (b.id / 1000 | 0)
    }))
      .attr('d', path)

  svg.append('path')
      .attr('class', 'state-boundaries')
      .datum(mesh(us, us.objects.states, function (a, b) { return a !== b }))
      .attr('d', path)
}

export const HomeView = () => (
  <div>
    {ready(null, usData, congressData)}
  </div>
)

export default HomeView
